﻿using System;
using System.IO;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}

static string[,] CreateArray(int rows, int cols)
{
    //Console.WriteLine($" Table Rows: {rows} Cols: {cols}");
    string[,] table = new string[rows, cols];
    return table;
}

static void listVehicles(string[,] carTable)
{
    for (int i = 0; i < carTable.GetLength(0); i++)
    {
        Console.WriteLine($"{carTable[i, 0]} - {carTable[i, 1]} {carTable[i, 2]}");
    }
}

static void listVehicleDetails(ref string[,] carTable, int carID, string inventoryFileName, ref int vehicleCnt)
{
    string[] headers = { "ID", "Make", "Model", "Color", "Odometer", "Price" };
    //Console.WriteLine($"0: {carTable.GetLength(0)}");
    //Console.WriteLine($"1: {carTable.GetLength(1)}");


    for (int j = 0; j < carTable.GetLength(1); j++)
    {
        try
        {
            Console.WriteLine($"{headers[j]}: {carTable[carID, j]}");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Vehicle not found");
            return;
        }
    }
    Console.WriteLine("Would you like to purchase?");
    bool ans = ToBoolFuzzy(Console.ReadLine());
    if (ans)
    {
        createInvoice(ref carTable, carID, inventoryFileName, ref vehicleCnt);
    }
}
static void createInvoice(ref string[,] carTable, int carID, string inventoryFileName, ref int vehicleCnt)
{
    DateTime d = DateTime.Now;
    Console.WriteLine($"Thank you for wanting to purchase the {carTable[carID, 1]} {carTable[carID, 2]}");
    Console.WriteLine("Please enter your name");
    string name = Console.ReadLine();
    string invoiceFileName = $"C:\\MCS Development\\CarDealership\\CarInvoiceFor{name}-{carID} - {d.Year}{d.Month:D2}{d.Day:D2}{d.Hour:D2}{d.Minute}.txt"; ;
    using (StreamWriter outputFile = new StreamWriter(invoiceFileName))
    {
        outputFile.WriteLine($"{d:MMMM d, yyyy} at {d:h:mm:ss}");
        outputFile.WriteLine($"{name} purchased a {carTable[carID, 3]} {carTable[carID, 1]} {carTable[carID, 2]} for {Convert.ToDecimal(carTable[carID, 5]):C}");
        outputFile.WriteLine($"Current odometer reading is {Convert.ToInt32(carTable[carID, 4]):N}");
    }
    Console.WriteLine($"Please retreive your invoice from {Path.GetDirectoryName(invoiceFileName)}");
    Console.WriteLine("");

    // Remove car bought
    string line = null;
    int carToRemove = carID + 1;
    string directory = Path.GetDirectoryName(inventoryFileName);
    string tempFile = directory + "\\tempInventory.txt";
    Console.WriteLine(tempFile);

    using (StreamReader reader = new StreamReader(inventoryFileName))
    {
        using (StreamWriter writer = new StreamWriter(tempFile))
        {
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith(carToRemove.ToString() + ","))
                    continue;

                writer.WriteLine(line);
            }
        }
    }
    // Make sure the file exists
    if (!File.Exists(tempFile))
    {
        Console.WriteLine($"Error: {tempFile} doesn't exist!");
        return;
    }
    // Rename the file
    File.Delete(inventoryFileName);
    File.Move(tempFile, inventoryFileName);

    // Reload Array
    vehicleCnt--;
    carTable = null;
    carTable = CreateArray(vehicleCnt, 6);
    loadTable(ref carTable, inventoryFileName);
}

static void addVehicle(ref string[,] carTable, ref int vehicleCnt, string inventoryFileName)
{
    string model;
    string make;
    string color;
    int mileage;
    decimal price;
    Console.WriteLine("Enter make of car");
    make = Console.ReadLine();
    Console.WriteLine("Enter model of car");
    model = Console.ReadLine();
    Console.WriteLine("Enter color of car");
    color = Console.ReadLine();
    Console.WriteLine("Enter mileage of car");
    mileage = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine("Enter price of car");
    price = Convert.ToDecimal(Console.ReadLine());

    vehicleCnt++;
    using (StreamWriter outputFile = new StreamWriter(inventoryFileName, true))
    {
        outputFile.WriteLine($"{vehicleCnt},{make},{model},{color},{mileage},{price}");
    }
    carTable = null;
    carTable = CreateArray(vehicleCnt, 6);
    loadTable(ref carTable, inventoryFileName);
}

static void loadTable(ref string[,] carTable, string inventoryFileName)
{
    // load cc
    try
    {
        int i = 0, j = 0;
        using (StreamReader inputFile = new StreamReader(inventoryFileName))
        {
            while (inputFile.EndOfStream != true)
            {
                string row = inputFile.ReadLine();
                //Console.WriteLine(i);
                //Console.WriteLine($"Row: {row}");
                j = 0;
                int elements = row.Trim().Split(',').Length;
                foreach (var col in row.Trim().Split(','))
                {
                    //Console.WriteLine(col);
                    carTable[i, j] = col.Trim();
                    j++;
                    //if (j >= elements)
                    //{
                    //    break;
                    //}
                }
                i++;
            }
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error opening file: {ex.Message}");
    }
}

bool tryAgain = true;
string menuAns = "";
string inventoryFileName = @"C:\MCS Development\CarDealership\inventory.txt";
int vehicleCnt = File.ReadLines(inventoryFileName).Count(); ;


string[,] carTable = CreateArray(vehicleCnt, 6);
loadTable(ref carTable, inventoryFileName);


do
{
    Console.WriteLine("Would you like to:");
    Console.WriteLine("1. View all vehicles");
    Console.WriteLine("2. Look up a vehicle by id");
    Console.WriteLine("3. Look up a vehicle by make/model");
    Console.WriteLine("4. Look up a vehicle by within a price range");
    Console.WriteLine("5. Look up a vehicle by color");
    Console.WriteLine("6. Add a vehicle");
    Console.WriteLine("7. Quit");
    menuAns = Console.ReadLine();
    if (menuAns != "1" && menuAns != "2" && menuAns != "3" && menuAns != "4"
        && menuAns != "5" && menuAns != "6" && menuAns != "7")
    {
        Console.WriteLine("Invalid choice, please try again");
        continue;
    }
    else if (menuAns == "1")
    {
        listVehicles(carTable);
    }
    else if (menuAns == "2")
    {
        Console.WriteLine("Enter car ID:");
        int carID = Convert.ToInt32(Console.ReadLine());
        listVehicleDetails(ref carTable, carID - 1, inventoryFileName, ref vehicleCnt);
    }
    else if (menuAns == "3")
    {
        string make;
        string model;
        int carID;
        bool found = false;
        Console.WriteLine("Enter car make:");
        make = Console.ReadLine();
        Console.WriteLine("Enter car model:");
        model = Console.ReadLine();
        for (int i = 0; i < carTable.GetLength(0); i++)
        {
            if (carTable[i, 1].ToLower().Contains(make.ToLower()) && carTable[i, 2].ToLower().Contains(model.ToLower()))
            {
                carID = Convert.ToInt32(carTable[i, 0]);
                listVehicleDetails(ref carTable, carID - 1, inventoryFileName, ref vehicleCnt);
                found = true;
            }
        }
        if (!found)
        {
            Console.WriteLine($"{make}/{model} is not currently available");
        }
    }
    else if (menuAns == "4")
    {
        decimal lowerLimit = 0;
        decimal upperLimit = 0;
        int carID;
        bool found = false;
        Console.WriteLine("Enter upper limit price:");
        upperLimit = Convert.ToDecimal(Console.ReadLine());
        Console.WriteLine("Enter lower limit price:");
        lowerLimit = Convert.ToDecimal(Console.ReadLine());
        for (int i = 0; i < carTable.GetLength(0); i++)
        {
            if (Convert.ToDecimal(carTable[i, 5]) >= lowerLimit && Convert.ToDecimal(carTable[i, 5]) <= upperLimit)
            {
                carID = Convert.ToInt32(carTable[i, 0]);
                listVehicleDetails(ref carTable, carID - 1, inventoryFileName, ref vehicleCnt);
                found = true;
            }
        }
        if (!found)
        {
            Console.WriteLine($"No cars between {upperLimit} - {lowerLimit} are currently available");
        }
    }
    else if (menuAns == "5")
    {
        string color;
        int carID;
        bool found = false;
        Console.WriteLine("Enter car color:");
        color = Console.ReadLine();

        for (int i = 0; i < carTable.GetLength(0); i++)
        {
            if (string.Compare(carTable[i, 3], color, true) == 0)
            {
                carID = Convert.ToInt32(carTable[i, 0]);
                listVehicleDetails(ref carTable, carID - 1, inventoryFileName, ref vehicleCnt);
                found = true;
            }
        }
        if (!found)
        {
            Console.WriteLine($"A {color} car is not currently available");
        }
    }
    else if (menuAns == "6")
    {
        addVehicle(ref carTable, ref vehicleCnt, inventoryFileName);
    }
    else if (menuAns == "7")
    {
        tryAgain = false;
    }


    //Console.Write("Try again? ");
    //try
    //{
    //    tryAgain = ToBoolFuzzy(Console.ReadLine());
    //}
    //catch (FormatException)
    //{
    //    Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
    //    tryAgain = true;
    //}
} while (tryAgain);