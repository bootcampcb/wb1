﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrubbing
{
    public class DataScrubber
    {
        public static void ScrubPhone(ref string phone)
        {
            phone = phone.Trim();
        }
        public static void ScrubPhone(ref string phone, string charToRemove)
        {
            string scrubbedPhone;
            scrubbedPhone = phone.Replace(charToRemove, "");
            phone = scrubbedPhone.Trim();
        }
        public static void ScrubPhone(ref string phone, string charToRemove, string charToReplaceWith)
        {
            string scrubbedPhone;
            scrubbedPhone = phone.Replace(charToRemove, charToReplaceWith);
            phone = scrubbedPhone.Trim();
        }
    }
}
