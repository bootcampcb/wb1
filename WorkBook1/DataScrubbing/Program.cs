﻿using DataScrubbing;
bool tryAgain = false;
string ans = null;
string charToRemove;
string charToReplaceWith ;

string phone;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}


do
{
    Console.WriteLine("Pick scrubbing method");
    Console.WriteLine("1: Trim white space");
    Console.WriteLine("2: Remove character(s)");
    Console.WriteLine("3: Replace character(s) with another");
    ans = Console.ReadLine();

    if (ans == "1")
    {
        Console.Write("Enter your phone number: ");
        phone = Console.ReadLine();
        DataScrubber.ScrubPhone(ref phone);
    }
    else if (ans == "2")
    {
        Console.Write("Enter your phone number: ");
        phone = Console.ReadLine();
        Console.Write("What character do you want to remove?");
        charToRemove = Console.ReadLine();
        DataScrubber.ScrubPhone(ref phone, charToRemove);
    }
    else if (ans == "3")
    {
        Console.Write("Enter your phone number: ");
        phone = Console.ReadLine(); 
        Console.Write("What character do you want to remove?");
        charToRemove = Console.ReadLine();
        Console.Write("What character do you want replace with?");
        charToReplaceWith = Console.ReadLine();
        DataScrubber.ScrubPhone(ref phone, charToRemove, charToReplaceWith);
    }
    else
    {
        continue;
    }

    Console.WriteLine($"Scrubbed phone number: {phone}");

    Console.Write("Try again? ");
    try
    {
        tryAgain = ToBoolFuzzy(Console.ReadLine());
    }
    catch (FormatException)
    {
        Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
        tryAgain = true;
    }
} while (tryAgain);