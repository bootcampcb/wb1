﻿DirectoryInfo di = new DirectoryInfo(@"C:\MCS Development\");
string completeFileName = @"C:\MCS Development\test.txt";
// All files
foreach (var fi in di.GetFiles("*"))
{
    Console.WriteLine(fi.Name);
}
if (File.Exists(completeFileName))
{
    Console.WriteLine("File already exists");
}
else
{
    File.Create(completeFileName);
}

// write to file
await File.WriteAllTextAsync(completeFileName, "To be or not to be");

DateTime d = DateTime.Now;
string folderName = Path.GetDirectoryName(completeFileName);
string fileName = Path.GetFileName(completeFileName);
string partFileName = fileName.Substring(0, fileName.LastIndexOf('.'));
string extension = Path.GetExtension(completeFileName);
Console.WriteLine(folderName);
Console.WriteLine(fileName);
Console.WriteLine(partFileName);
Console.WriteLine(extension);


string newFileName = $"{folderName}\\{partFileName}-backup-{d.Year}{d.Month:D2}{d.Day:D2}{d.Hour:D2}{d.Minute}{d.Second}{extension}";
Console.WriteLine(newFileName);
File.Copy(completeFileName, newFileName, true);
