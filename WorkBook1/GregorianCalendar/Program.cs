﻿int year;

Console.Write("Enter a year: ");
year = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("Year = " + year);
if (DateTime.IsLeapYear(year))
{
    Console.WriteLine("Leap Year!");
}
else
{
    Console.WriteLine("Not a Leap Year!");
}