﻿int hour;
int minute;
int incMinutes;

Console.Write("Enter hour (0-23): ");
hour = Convert.ToInt32(Console.ReadLine());
Console.Write("Enter minutes past hour(0-59: ");
minute = Convert.ToInt32(Console.ReadLine());
Console.Write("Increment time by this many minutes: ");
incMinutes = Convert.ToInt32(Console.ReadLine());

DateTime startTime = DateTime.Today.AddHours(hour).AddMinutes(minute);//new DateTime(2013,9,6,18,40,0,DateTimeKind.Local);//DateTime.Today.AddHours(10).AddMinutes(50);
DateTime endTime = startTime.AddMinutes(incMinutes);//DateTime.Now;

Console.WriteLine($"New time: {endTime}");
