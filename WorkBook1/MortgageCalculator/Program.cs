﻿double A;
double MIR;
double NP;
double Pmt;
double lifeOfLoan;
double estInterest;

string command = "";
do
{
    Console.WriteLine("Commands -->");
    Console.WriteLine(" QUOTE : Place an order");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine().ToUpper();

    if (command == "QUOTE")
    {
        try
        {
            Console.Write("How much are you borrowing? ");
            A = Convert.ToDouble(Console.ReadLine());
            Console.Write("What is your interest rate? ");
            MIR = Convert.ToDouble(Console.ReadLine()) / 1200;
            Console.Write("How long is your loan (in years)? ");
            NP = Convert.ToDouble(Console.ReadLine()) * 12;

            Pmt = (A * MIR) / (1 - Math.Pow((1 + MIR), -NP));
            lifeOfLoan = Pmt * NP;
            estInterest = (lifeOfLoan - A);

            Console.WriteLine($"Your estimated payment is {Pmt:C} ");
            Console.WriteLine($"You paid {lifeOfLoan:C} over the life of the loan");
            Console.WriteLine($"Your total interest cost for the loan was {estInterest:C}");
        }
        catch (Exception ex)
        {

            //Console.WriteLine(ex.ToString());
            Console.WriteLine($"Error: {ex.Message}");

        }
    }
    else if (command != "QUIT")
    {
        Console.WriteLine($"{command} not found");
    }
} while (command != "QUIT");