﻿static int[,] CreateArray()
{
    int[,] table = new int[5, 10];
    return table;
}

int[,] mathTable = CreateArray();
string div = "---";
for (int i = 0; i < mathTable.GetLength(0); i++) 
{
    for (int j = 0; j < mathTable.GetLength(1); j++)
    {
        mathTable[i, j] = i * j; // multiplication table
    }
}


// Header
Console.Write($"  {"",6}");
for (int i = 0; i < mathTable.GetLength(0); i++)
{
    Console.Write($"{i,6}");
}
Console.WriteLine();

// Separator
Console.Write($"  {"",6}");
for (int i = 0; i < mathTable.GetLength(0); i++)
{
    Console.Write($"{div,6}");
}
Console.WriteLine();


for (int j = 0; j < mathTable.GetLength(1); j++)
{
    Console.Write($"{j,6} |");
    for (int i = 0; i < mathTable.GetLength(0); i++)
    {
        Console.Write($"{mathTable[i, j],6}");
    }
    Console.WriteLine();
}
