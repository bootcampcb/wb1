﻿bool tryAgain = false;
string name;
string firstName;
string middleName;
string lastName;
int pos1;
int pos2;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}


do
{

    Console.WriteLine("Enter a full name");
    name = Console.ReadLine();

    pos1 = name.IndexOf(" ");
    pos2 = name.LastIndexOf(" ");

    if (pos1 == -1)
    {
        firstName = name;
    }
    else
    {
        firstName = name.Substring(0, pos1);
    }


    if (pos2 < 0)
    {
        lastName = "<undefined>";
    }
    else
    {
        lastName = name.Substring(pos2 + 1);
    }

    if (pos1 == pos2)
    {
        middleName = "<undefined>";
    }
    else
    {
        middleName = name.Substring(pos1 + 1, pos2 - pos1);
    }

    Console.WriteLine($"Name: {name}");
    Console.WriteLine($"First name: {firstName}");
    Console.WriteLine($"Middle name: {middleName}"); 
    Console.WriteLine($"Last name: {lastName}");

    Console.Write("Try again? ");
    try
    {
        tryAgain = ToBoolFuzzy(Console.ReadLine());
    }
    catch (FormatException)
    {
        Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
        tryAgain = true;
    }
} while (tryAgain);