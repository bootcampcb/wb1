﻿using System;
using System.IO;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}
static decimal? GetPrice(string[] items, decimal[] prices, string str)
{
    for (int i = 0; i < items.Length; i++)
    {
        if (string.Compare(items[i], str, true) == 0)
        {
            return prices[i];
        }
    }
    return null;
}

bool tryAgain = false;
DateTime d = DateTime.Now;
string folderName = @"C:\MCS Development\";
string fileName;
string logFileName = @"C:\MCS Development\PayrollFileProcessor.log";
string payRollRpt = $"{folderName}payrollrpt-{d.Year}{d.Month:D2}{d.Day:D2}{d.Hour:D2}{d.Minute}{d.Second}.txt";
string id;
string name;
decimal rate;
decimal hrsWorked;
decimal grossPay;
decimal totFileGrossPay;
//string partFileName = fileName.Substring(0, fileName.LastIndexOf('.'));
//string extension = Path.GetExtension(completeFileName);

do
{
    Console.WriteLine("Enter a payroll file:");
    fileName = Console.ReadLine();
    fileName = folderName + fileName;
    totFileGrossPay = 0;

    try
    {
        using (StreamReader inputFile = new StreamReader(fileName))
        {
            while (inputFile.EndOfStream != true)
            {
                string text = inputFile.ReadLine();
                //Console.WriteLine(text);
                //Console.WriteLine(text.Substring(0, 5));
                //Console.WriteLine(text.Substring(5, 20));
                //Console.WriteLine(text.Substring(25, 5));
                //Console.WriteLine(text.Substring(31));

                id = text.Substring(0, 5);
                name = text.Substring(5, 20);
                rate = Convert.ToDecimal(text.Substring(25, 5));
                hrsWorked = Convert.ToDecimal(text.Substring(31));
                grossPay = rate * hrsWorked;
                totFileGrossPay += grossPay;
            }
        }
        Console.WriteLine($"{fileName} was processed");
        using (StreamWriter outputFile = new StreamWriter(logFileName, true))
        {
            outputFile.WriteLine($"Procesing file: {fileName} on {DateTime.Now:MM-dd-yyyy} at {DateTime.Now:h:mm:ss}");
            outputFile.WriteLine($"Gross pay totals were {totFileGrossPay:C}");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error opening file: {ex.Message}");
    }


    Console.Write("Try again? ");
    try
    {
        tryAgain = ToBoolFuzzy(Console.ReadLine());
    }
    catch (FormatException)
    {
        Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
        tryAgain = true;
    }
} while (tryAgain);