﻿string productCode = "";
int qty = 0;
double perUnitPrice = 0;
double totalPriceOrder;
Boolean discount = false;

string command = "";
do
{
    Console.WriteLine("Commands -->");
    Console.WriteLine(" GET-QUOTE : Place an order");
    Console.WriteLine(" QUIT : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine().ToUpper();

    if (command == "GET-QUOTE")
    {
        Console.Write("Enter production code(BG-127, WRTR-28 or QUAC-8): ");
        productCode = Console.ReadLine();
        Console.Write("Enter quantity: ");
        qty = Convert.ToInt32(Console.ReadLine());

        switch (productCode)
        {
            case "BG-127":
                {
                    if (qty >= 1 && qty <= 24)
                    {
                        perUnitPrice = 18.99;
                    }
                    else if (qty >= 25 && qty <= 50)
                    {
                        perUnitPrice = 17.00;
                    }
                    else if (qty >= 51)
                    {
                        perUnitPrice = 14.49;
                    }
                    break;
                }
            case "WRTR-28":
                {
                    if (qty >= 1 && qty <= 24)
                    {
                        perUnitPrice = 125.00;
                    }
                    else if (qty >= 25 && qty <= 50)
                    {
                        perUnitPrice = 113.75;
                    }
                    else if (qty >= 51)
                    {
                        perUnitPrice = 99.99;
                    }

                    break;
                }
            case "GUAC-8":
                {
                    if (qty >= 1 && qty <= 24)
                    {
                        perUnitPrice = 8.99;
                    }
                    else if (qty >= 25 && qty <= 50)
                    {
                        perUnitPrice = 8.99;
                    }
                    else if (qty >= 51)
                    {
                        perUnitPrice = 7.49;
                    }

                    break;
                }
            default:
                {
                    Console.WriteLine($"{productCode} product not found");
                    break;
                }
        }


        totalPriceOrder = (double)qty * perUnitPrice;
        if (qty > 250)
        {
            totalPriceOrder = totalPriceOrder * .85;
            discount = true;
        }

        if (totalPriceOrder > 0)
        {
            Console.WriteLine($"{productCode} {qty} {perUnitPrice:C}  Total price of order {totalPriceOrder:C}  discount applied: {discount}");
        }
    }
    else if (command != "QUIT")
    {
        Console.WriteLine($"{command} not found");
    }
} while (command != "QUIT");