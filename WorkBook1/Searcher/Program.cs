﻿string[] items = { "Sausage Breakfast Taco","Potato and Egg Breakfast Taco", "Sausage and Egg Biscuit",
 "Bacon and Egg Biscuit", "Pancakes"};
decimal[] prices = { 3.99M, 3.29M, 3.70M, 3.99M, 4.79M };

bool tryAgain = false;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}
static decimal? GetPrice(string[] items, decimal[] prices, string str)
{
    for (int i = 0; i < items.Length; i++)
    {
        if (string.Compare(items[i], str, true) == 0)
        {
            return prices[i];
        }
    }
    return null;
}

do
{
    Console.WriteLine("Sausage Breakfast Taco");
    Console.WriteLine("Potato and Egg Breakfast Taco");
    Console.WriteLine("Sausage and Egg Biscuit");
    Console.WriteLine("Bacon and Egg Biscuit");
    Console.WriteLine("Pancakes");
    Console.WriteLine("What would you like to order?");
    String str = Console.ReadLine();
    decimal? price = GetPrice(items, prices, str);
    
    if (price != null)
    {
        Console.WriteLine($"That will be {price:C}");
    }
    else
    {
        Console.WriteLine("Please pick something off the menu!");
    }

    Console.Write("Try again? ");
    try
    {
        tryAgain = ToBoolFuzzy(Console.ReadLine());
    }
    catch (FormatException)
    {
        Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
        tryAgain = true;
    }
} while (tryAgain);