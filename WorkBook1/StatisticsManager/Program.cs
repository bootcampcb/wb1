﻿double[] scores = { 197, 202, 148, 212, 137, 179, 186 };

static double GetAverage(double[] scores)
{
    double sum = 0;
    foreach (var score in scores)
    {
        sum += score;   
    }
    return sum / (double) scores.Length;
}
static double GetMean(double[] scores)
{
    Array.Sort(scores);
    if (scores.Length % 2 > 0)
    {
        return scores[(scores.Length / 2) + 1];
    }
    else
    {
        return (scores[(scores.Length / 2) + 1] + scores[(scores.Length / 2) + 1]) / 2;
    }

}
static double GetLargestValue(double[] scores)
{
    Array.Sort(scores);
    Array.Reverse(scores);
    //    scores.Reverse();
    return scores[0];
}
static double GetSmallestValue(double[] scores)
{
    Array.Sort(scores);
    return scores[0];
}


Console.WriteLine($"Average score: {GetAverage(scores)}");
Console.WriteLine($"Mean score: {GetMean(scores)}");
Console.WriteLine($"High score: {GetLargestValue(scores)}");
Console.WriteLine($"Low score: {GetSmallestValue(scores)}");