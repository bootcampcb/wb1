﻿bool tryAgain = false;
string name;
string major;
string majorName;
string classification;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}
static void GetData(out string name, out string major, out string classification)
{
    Console.Write("What is the student's name: ");
    name = Console.ReadLine();
    Console.Write("What is the student's major(BIOL,CSCI, ENG, HIST and MKT): ");
    major = Console.ReadLine().ToUpper();
    Console.Write("What is the student's classification (Freshman, Sophomore, Junior or Senior: ");
    classification = Console.ReadLine().ToUpper();
}
static string GetAdvisingLocation(string major, string classification, out string majorName)
{
    switch (major)
    {
        case "BIOL":
            {
                majorName = "Biology";
                if (classification == "FRESHMAN" || classification == "SOPHOMORE")
                {
                    return "Science Bldg, Room 310";
                }
                else
                {
                    return "Science Bldg, Room 311";
                }
            }
        case "CSCI":
            {
                majorName = "Computer Science";
                return "Sheppard Hall, Room 314";
            }
        case "ENG":
            {
                majorName = "English";
                if (classification == "FRESHMAN")
                {
                    return "Kerr Hall, Room 201";
                }
                else
                {
                    return "Kerr Hall, Room 312";
                }
            }
        case "HIST":
            {
                majorName = "History";
                return "Kerr Hall, Room 114";
            }
        case "MKT":
            {
                majorName = "Marketing";
                if (classification == "SENIOR")
                {
                    return "Westly Hall, Room 313";
                }
                else
                {
                    return "Westly Hall, Room 310";
                }
            }
        default:
            {
                majorName = "Not declared";
                return "Wrong School";
            }
    }
}


string advisingStr = "";
do
{
    GetData(out name, out major, out classification);
    advisingStr = GetAdvisingLocation(major, classification, out majorName);

    Console.WriteLine($"Advising for {majorName} {classification} majors: {advisingStr}");

    Console.Write("Try again? ");
    try
    {
        tryAgain = ToBoolFuzzy(Console.ReadLine());
    }
    catch (FormatException)
    {
        Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
        tryAgain = true;
    }
} while (tryAgain);
