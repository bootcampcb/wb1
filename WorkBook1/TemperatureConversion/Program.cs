﻿double fahrenheit;
double celsius;


static double CtoF(double celsius)
{
    return (celsius * 9) / 5 + 32;
}
static double FtoC(double fahrenheit)
{
    return (fahrenheit - 32) * 5 / 9;
}


string command = "";
do
{
    Console.WriteLine("Commands -->");
    Console.WriteLine(" 1 : Celsius to Feahrenheit");
    Console.WriteLine(" 2 : Feahrenheit to Celsius");
    Console.WriteLine(" 3 : Quit the program");
    Console.Write("Enter your command: ");
    command = Console.ReadLine();

    if (command == "1")
    {
        Console.Write("Please enter the temp in celcius: ");
        try
        {
            celsius = Convert.ToDouble(Console.ReadLine());
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            continue;
        }

        fahrenheit = CtoF(celsius);
        Console.WriteLine($"Celsius: {celsius} = {fahrenheit} Fahrenheit");
    }
    else if (command == "2")
    {
        Console.Write("Please enter the temp in fahrenheit: ");
        try
        {
            fahrenheit = Convert.ToDouble(Console.ReadLine()); ;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
            continue;
        }
        celsius = FtoC(fahrenheit);
        Console.WriteLine($"Fahrenheit: {fahrenheit} = {celsius} Celsius");
    }
    else if (command != "3")
    {
        Console.WriteLine($"{command} is invalid");
    }
} while (command != "3");