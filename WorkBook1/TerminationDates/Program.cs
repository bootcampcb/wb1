﻿DateTime gracePeriodDate;
DateTime lackOfPmtDate;
bool tryAgain = false;

static bool ToBoolFuzzy(string stringVal)
{
    string normalizedString = (stringVal?.Trim() ?? "false").ToLowerInvariant();
    bool result = (normalizedString.StartsWith("y")
        || normalizedString.StartsWith("t")
        || normalizedString.StartsWith("1"));
    return result;
}

do
{
    Console.WriteLine("Enter insurance policy renewal date");
    String str = Console.ReadLine();
    DateTime renewalDate = Convert.ToDateTime(str);

    gracePeriodDate = renewalDate.AddDays(10);
    lackOfPmtDate = renewalDate.AddMonths(1);

    Console.WriteLine($"You have until {gracePeriodDate} before you start accruing interest");
    Console.WriteLine($"You have until {lackOfPmtDate} before your policy is cancelled for lack of payment");

    Console.Write("Try again? ");
    try
    {
        tryAgain = ToBoolFuzzy(Console.ReadLine());
    }
    catch (FormatException)
    {
        Console.WriteLine($"Unable to convert '{tryAgain}' to a Boolean.\n");
        tryAgain = true;
    }
} while (tryAgain);